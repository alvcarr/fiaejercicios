%%%%%%%%Ejercicio 1%%%%%%%%%%%%%.
dame_elementoSum(X,Sum):-Sum is X.
suma_elementos([],1).
suma_elementos([Cabeza|Cola],Sum):-dame_elementoSum(Cabeza,SumEle),suma_elementos(Cola,Sum1),Sum is Sum1+SumEle.
eliminaAparicion(_,[_],[]):-!.
eliminaAparicion(X,[X|Cola],Cola).
eliminaAparicion(X,[Y|Cola],[Y|Res]):-eliminaAparicion(X,Cola,Res).
maximo([A],A).
maximo([A,B|Cola],Res):-A<B,maximo([B|Cola],Res),!.
maximo([A,B|Cola],Res):-A>B,maximo([A|Cola],Res),!.
%%%%%%%%Ejercicio 2%%%%%%%%%%
%Sintaxis de utilización: ?- pasaBinario(NÚMERO EN DECIMAL,[],Respuesta).
pasaBinario(0,Lista,[Lista]):-!.
pasaBinario(Valor,Lista,Res):-NuevoValor is Valor//2, Resto is Valor mod 2,pasaBinario(NuevoValor,[Resto|Lista],Res).

%%%%Ejercicio 3%%%

%%%Sintaxis de utilización del programa: Ej: dameTotal(persona(pedro,_,S),M,H,Money).


familia(persona(juan,perez,5000),
		persona(maria,alonso,10000),
		[persona(carlos,perez,0),
		persona(andres,perez,0),
		persona(elena,perez,200)]).
familia(persona(pedro,lopez,25000),
		persona(carmen,ruiz,15000),
		[persona(carlos,lopez,1000),
		persona(teresa,lopez,0)]).
dameSueldo(persona(_,_,S),S):-!.

sueldoHijos([],0).
sueldoHijos([Primero|Resto],Total):-dameSueldo(Primero,Sa),sueldoHijos(Resto,Sb), Total is Sa + Sb.


dameTotal(Marido,Esposa,Hijos,Money):-familia(Marido,Esposa,Hijos),dameSueldo(Marido,MoneyA),dameSueldo(Esposa,MoneyB),sueldoHijos(Hijos,MoneyC), Money is MoneyA+MoneyB+MoneyC,!.
