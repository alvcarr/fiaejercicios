es_jefe(directorgeneral,responsablecalidad).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
es_jefe(responsablecalidad,responsabletesoreria).
es_jefe(responsablecalidad,responsableventas).
es_jefe(responsablecalidad,responsablecomercial).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
es_jefe(responsabletesoreria,asesor).
es_jefe(asesor,contable).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
es_jefe(responsableventas,operario1).
es_jefe(responsableventas,operario2).
es_jefe(operario1,reparto1).
es_jefe(operario2,reparto2).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
es_jefe(responsablecomercial,vendedor1).
es_jefe(responsablecomercial,vendedor2).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

que_jefe(X,Y):-es_jefe(Y,X).
que_jefe(X,Z):-es_jefe(Y,X),que_jefe(Y,Z).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cn(X,Y,Z):-es_jefe(Y,X),Z is 1.
cn(X,Y,V):-cn(X,Z,J),cn(Z,Y,K),V is K+J.
cuantos_niveles(X,Y,Z):-cn(X,Y,Z).
