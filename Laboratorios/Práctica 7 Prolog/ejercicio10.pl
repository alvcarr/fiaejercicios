descendiente(paginaprincipal,formacion).
descendiente(paginaprincipal,integrarsistemas).
%%%%%%%%%%%%%%%%%%%%%%%%
descendiente(formacion,creatividad).
descendiente(formacion,innovacion).
descendiente(creatividad,quees).
descendiente(creatividad,guia).
descendiente(guia,queesunaidea).
descendiente(guia,comotenerideas).
%%%%%%%%%%%%%%%%%%%%%%%%%
descendiente(integrarsistemas,calidad).
descendiente(integrarsistemas,mambiente).
descendiente(calidad,iso).
descendiente(iso,sistemagestion).
descendiente(iso,recursos).
%%%%%%%%%%%%%%%%%%%%%%%%%
ascendiente(X,Y):-descendiente(Y,X).

cuales_cuelgan(X,Y):-descendiente(X,Y).
cuales_cuelgan(X,Y):-descendiente(X,Z),cuales_cuelgan(Z,Y).

peso(X,Y,Z):-descendiente(X,Y),Z is 1.
peso(X,Y,M):-descendiente(X,Z),peso(Z,Y,L), M is L+1.

cuales_suben(X,Y):-ascendiente(X,Y).
cuales_suben(X,Y):-ascendiente(X,Z),cuales_suben(Z,Y).

como_ir(X,Y):-cuales_suben(X,Y).
como_ir(X,Y):-cuales_cuelgan(X,Y).
