signo(aries,20,3,19,4).
signo(tauro,20,4,20,5).
signo(geminis,21,5,20,6).
signo(cancer,21,6,22,7).
signo(leo,23,7,22,8).
signo(virgo,23,8,22,9).
signo(libra,23,9,22,10).
signo(escorpio,23,10,21,11).
signo(sagitario,22,11,21,12).
signo(capricornio,22,12,19,1).
signo(acuario,20,1,17,2).
signo(piscis,18,2,19,3).
mes(1,31).
mes(2,28).
mes(3,31).
mes(4,30).
mes(5,31).
mes(6,30).
mes(7,31).
mes(8,31).
mes(9,30).
mes(10,31).
mes(11,30).
mes(12,31).
dameSigno(S,DD,MM):-mes(MM,NUMERODIAS),DD=<NUMERODIAS,signo(S,D0,M0,D1,M1),((D0=<DD,M0 is MM);(D1>=DD,M1 is MM)),!.

