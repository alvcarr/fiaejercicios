precio_transporte(roma,200).
precio_transporte(londres,250).
precio_transporte(tunez,150).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
precio_alojamiento(roma,hotel,250).
precio_alojamiento(londres,hotel,150).
precio_alojamiento(tunez,hotel,100).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
precio_alojamiento(roma,hostal,150).
precio_alojamiento(londres,hostal,100).
precio_alojamiento(tunez,hostal,80).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
precio_alojamiento(roma,camping,100).
precio_alojamiento(londres,camping,50).
precio_alojamiento(tunez,camping,50).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

coste(CIUDAD,MODOALOJAMIENTO,NOCHES,PRECIO):-precio_alojamiento(CIUDAD,MODOALOJAMIENTO,PRECIOA),precio_transporte(CIUDAD,PRECIOB),PRECIO is (PRECIOA + PRECIOB)*NOCHES.

