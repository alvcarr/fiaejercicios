potencia(A,0,1):- A =\=0.  % validamos que A se a diferente de 0 pues 0^0 es indeterminado.
potencia(X,Y,P):- Y>0,Y1 is Y-1,potencia(X,Y1,P1),P is X*P1.