%%%%%%%%Ejercicio 1%%%%%%%%%%%%%
es_lista(X):-X=[].
es_lista(X):-X=[_|_].
%%%%%%%%Ejercicio 2%%%%%%%%%%%%%
dame_elementoInc(X,Sum):-Sum is 1.
calcula_tam([],0).
calcula_tam([A|S],Sum):-dame_elementoInc(X,SumE),calcula_tam(S,Sum1),Sum is SumE+Sum1.
%%%%%%%%%%%%%Ejercicio 3 %%%%%%%%
dame_elementoSum(X,Sum):-Sum is X.
suma_elementos([],1).
suma_elementos([Cabeza|Cola],Sum):-dame_elementoSum(Cabeza,SumEle),suma_elementos(Cola,Sum1),Sum is Sum1+SumEle.
%%%%%%%%%%%%Ejercicio 4%%%%%%%
eliminaAparicion(X,[X|Cola],Cola).
eliminaAparicion(X,[Y|Cola],[Y|Res]):-eliminaAparicion(X,Cola,Res).
%%%%%%%%%%%%Ejercicio 5%%%%%%%%
insertar(X,Lista,[X|Lista]).
insertar(X,[Y|Lista],[Y|Res]):-insertar(X,Lista,Res).
%%%%%%%%%%%Ejercicio 6 %%%%%%%%
divideListaB([X|Y],B):-append(X,Y,B).
borraSecuenciaA(Secuencia,[Secuencia|Cola],[]).
borraSecuenciaA(Secuencia,[Y|Cola],Y|Resultado):-borraSecuenciaA(Secuencia,Cola,Resultado).

borraSecuenciaB(Secuencia,Lista,Res):-append([Secuencia],Res,Lista).
borraSecuenciaB(Secuencia,[Cabeza|Cola],Cabeza|Res):-borraSecuenciaB(Secuencia,Cola,Res).

borrarTresPrimeros([A,B,C|Res],Res).
borrarTresUltimos(Lista,A):-append(A,[_,_,_],Lista).
es_miembroAppend(X,Lista):-append([X],A,Lista).
es_miembroAppend(X,[Cabeza|Cola]):-es_miembroAppend(X,Cola).
extraeUltimoElementoCon(Lista,Res):-append(Res,[_],Lista).
extraeUltimoElementoSin([_],[]).
extraeUltimoElementoSin([Cabeza|Cola],Cabeza|Res):-extraeUltimoElementoSin(Cola,Res).
%%%%%%%%%%%Ejercicio 7%%%%%%%
dameElementos(X,[X|Lista],[]).
dameElementos(X,[Y|Lista],[Y|Resultado]):-dameElementos(X,Lista,Resultado).
%%%%%%%%%%Ejercicio 8%%%%%%
es_mayor(X,Y):-Y>X.
ordenCreciente([]).
ordenCreciente([A,B|Cola]):-es_mayor(A,B),ordenCreciente(Cola).
%%%%%%%%%%Ejercicio 9%%%%%%
es_menor(X,Y):-Y<X.
ordenDecreciente([]).
ordenDecreciente([A,B|Cola]):-es_menor(A,B),ordenDecreciente(Cola).
%%%%%%%%%%Ejercicio 10%%%%%% PENDIENTE DE REALIZAR
%ponOrdenCreciente(Num,[],Res).
%ponOrdenCreciente(Num,[Num,Segundo|Cola],[Num,Segundo|Res]):-es_mayor(Num,Segundo),ponOrdenCreciente(Num,Cola,Res).
%ponOrdenCreciente(Num,[Num,Segundo|Cola],[Segundo,Num|Res]):-es_menor(Num,Segundo),ponOrdenCreciente(Num,Cola,Res).
%%%%%%%%%%Ejercicio 11%%%%%%  PENDIENTE DE REALIZAR
%%%%%%%%%Ejercicio 12%%%%%%%%%%%%%% PENDIENTE DE REALIZAR
eliminaEnesima([A0,A1,A2,A3,A4,A5,A6,A7,A8|Cola],A0|A1|A2|A3|A4|A5|A6|A7|Cola).
%%%%%%%%%%Ejercicio 13%%%%%%%%%
son_consecutivos(X,Y):-Y is X+1.
%%%%%%%%%%Ejercicio 14%%%%%%%
prefijo([X|Lista],X).

sufijo([A],A).
sufijo([Cabeza|Cola],Res):-sufijo(Cola,Res).

sublista([X|Cola],X|Cola).
sublista([Cabeza|Cola],Cabeza|Resultado):-sublista(Cola,Resultado).

%%%%%%Ejercicio 15 %%%%%%%%
reverse([],[]).
reverse([A,B],Res):-append([B],[A],Res).
reverse([Cab|Lista],Res|Cab):-reverse(Lista,Res).

adyacentes(X,Y,Xs):-append([X,Y],_,Xs).

ultimo(Xs,X):-append(_,[X],Xs).

primeros(Xs,Ys):-append(Ys,[_],Xs).

%%%%%%%Ejercicio 16%%%%%%%%
maximo([A],A).
maximo([A,B|Cola],Res):-A<B,maximo([B|Cola],Res),!.
maximo([A,B|Cola],Res):-A>B,maximo([A|Cola],Res),!.
