familia(persona(juan,perez,43),
		persona(maria,alonso,41),
		[persona(carlos,perez,19),
		persona(andres,perez,30000000000000000000000000000),
		persona(elena,perez,100)]).
edad(persona(_,_,EDAD),EDAD).

dameCabeza([Cabeza|Cola],Cabeza).
dameCola([Cabeza|Cola],Cola).


mediaAritmetica([],0):-!.
mediaAritmetica([Cabeza|Cola],Resultado):-edad(Cabeza,EC),mediaAritmetica(Cola,Results),Resultado is Results+EC.

calculaTAM([],0).
calculaTAM([Cabeza|Cola],Res):-calculaTAM(Cola,Res0), Res is Res0+1.

getMedia([Cabeza|Cola],Res):-mediaAritmetica([Cabeza|Cola],Res2),calculaTAM([Cabeza|Cola],NUM),Res is Res2/NUM.


getMayor([Mayor],Mayor).
getMayor([A,B|Cola],Resultao):-edad(A,Tmp0),edad(B,Tmp1),Tmp1>Tmp0,getMayor([B|Cola],Resultao),!.
getMayor([A,B|Cola],Resultao):-edad(A,Tmp0),edad(B,Tmp1),Tmp0>=Tmp1,getMayor([A|Cola],Resultao),!.
