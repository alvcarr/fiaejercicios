/*
  
	Examen Ordinario de Prolog
	DNI - XX
	1 de Junio de 2016.	

    El predicado familia tiene como argumentos los miembros de una familia. El primero es el padre, el segundo la madre y el tercero
    es la lista de hijos. Cada componente de la familia se expresa mediante la función persona(Nombre, Apellido1, Sueldo).*/

familia(persona(juan,perez,50),
	persona(maria,alonso,10),
	[persona(carlos,perez,0),
	persona(andres,perez,0),
	persona(elena,perez,2)]).

familia(persona(pedro,lopez,25),
	persona(carmen,ruiz,15),
	[persona(carlos,lopez,10),
	persona(teresa,lopez,0)]).

familia(persona(carlos,lopez,2),
	persona(lola,garcia,1),
	[]).

familia(persona(jorge,gonzalez,17),
	persona(lola,fernandez,35),
	[persona(isabel,gonzalez,12),
	persona(fernando,gonzalez,0)]).

sueldo(persona(_,_,S),S).

/* Apartado A. Obtener las familias que tienen un número de hijos par o impar
      Ejemplo de consulta: 
         ?- familia(M0,M1,Hijos),numeroPar(Hijos). para obtener las familias con un numero par de hijos. Devuelve:
	
	M0 = persona(pedro, lopez, 25),
	M1 = persona(carmen, ruiz, 15),
	Hijos = [persona(carlos, lopez, 10), persona(teresa, lopez, 0)] ;
	
	M0 = persona(carlos, lopez, 2),
	M1 = persona(lola, garcia, 1),
	Hijos = [] ;

	M0 = persona(jorge, gonzalez, 17),
	M1 = persona(lola, fernandez, 35),
	Hijos = [persona(isabel, gonzalez, 12), persona(fernando, gonzalez, 0)].


         familia(M0,M1,Hijos),numeroImpar(Hijos). para obtener las familias con un numero impar de hijos.

	M0 = persona(juan, perez, 50),
	M1 = persona(maria, alonso, 10),
	Hijos = [persona(carlos, perez, 0), persona(andres, perez, 0), persona(elena, perez, 2)] ;
	
	false.


*/

cuantosHijos([],0).
cuantosHijos([Cabeza|Cola],Resultado):-cuantosHijos(Cola,Tmp0), Resultado is Tmp0+1.

numeroPar(Hijos):-cuantosHijos(Hijos,NumHijos),0 is NumHijos mod 2.
numeroImpar(Hijos):-cuantosHijos(Hijos,NumHijos),not(0 is NumHijos mod 2).


/* Apartado B. Obtener una lista con los hijos de una familia que tienen sueldo
    Ejemplo de consulta:

	?- familia(M0,M1,Hijos),hijosSueldo(Hijos,Respuesta).  que devuelve, para cada familia, la lista de hijos que tienen sueldo.

	M0 = persona(juan, perez, 50),
	M1 = persona(maria, alonso, 10),
	Hijos = [persona(carlos, perez, 0), persona(andres, perez, 0), persona(elena, perez, 2)],
	Respuesta = [persona(elena, perez, 2)] ;

	M0 = persona(pedro, lopez, 25),
	M1 = persona(carmen, ruiz, 15),
	Hijos = [persona(carlos, lopez, 10), persona(teresa, lopez, 0)],
	Respuesta = [persona(carlos, lopez, 10)] ;
	
	M0 = persona(carlos, lopez, 2),
	M1 = persona(lola, garcia, 1),
	Hijos = Respuesta, Respuesta = [] ;

	M0 = persona(jorge, gonzalez, 17),
	M1 = persona(lola, fernandez, 35),
	Hijos = [persona(isabel, gonzalez, 12), persona(fernando, gonzalez, 0)],
	Respuesta = [persona(isabel, gonzalez, 12)] ;
	false

	O bien, 
	
	?-familia(persona(juan,_,_),M1,Hijos),hijosSueldo(Hijos,Respuesta).

	M1 = persona(maria, alonso, 10),
	Hijos = [persona(carlos, perez, 0), persona(andres, perez, 0), persona(elena, perez, 2)],
	Respuesta = [persona(elena, perez, 2)] ;
	false.

	Si quisiera consultar los hijos de una determinada familia.

*/


hijosSueldo([],[]).
hijosSueldo([Cabeza|Cola],Tmp1):-sueldo(Cabeza,Tmp0),Tmp0 is 0,hijosSueldo(Cola,Tmp1).
hijosSueldo([Cabeza|Cola],[Cabeza|Tmp1]):-sueldo(Cabeza,Tmp0),Tmp0>0,hijosSueldo(Cola,Tmp1).


