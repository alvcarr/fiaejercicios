/* 

	Examen ordinario de Prolog.
	DNI- XX
	1 de Junio de 2016.


   Predicados utilizados: 
   pertenece(X,Y): X pertenece a la familia Y.
*/


pertenece(wv2010,word).
pertenece(wv2007,word).
pertenece(wv2003,word).

pertenece(pv2010,powerpoint).
pertenece(pv2007,powerpoint).
pertenece(pv2003,powerpoint).

pertenece(uv2010,publisher).
pertenece(uv2007,publisher).
pertenece(uv2003,publisher).

pertenece(vv2010,visualstudio).
pertenece(vv2012,visualstudio).

pertenece(nv691,netbeans).
pertenece(nv701,netbeans).
pertenece(nv712,netbeans).

pertenece(sv625w,prolog).
pertenece(sv625m,prolog).

pertenece(word,procesadortxt).
pertenece(powerpoint,publicidad).
pertenece(publisher,publicidad).
pertenece(visualstudio,ide).
pertenece(netbeans,ide).
pertenece(prolog,entprolog).

pertenece(procesadortxt,oficina).
pertenece(publicidad,oficina).
pertenece(ide,programacion).
pertenece(entprolog,programacion).

pertenece(oficina,herramientas).
pertenece(programacion,herramientas).

/*Consultas realizables

	perteneceCategoria(X,Y), dice si determinado item pertenece o no a determinada categoria. En caso de no definir Y, lista todas aquellas a las que pertenece.
	Ejemplo de consulta: ?- perteneceCategoria(wv2010,X), que devuelve word, procesadortxt,oficina,herramientas.

	?- perteneceCategoria(pv2007,oficina). que devuelve true, ya que pertenece a esta.

	Para ver el software que pertenece a una determinada categoría:
	?-	perteneceCategoria(X,oficina). , que devuelve todos los items de la categoria oficina (procesadortxt,publicidad,wv2010,wv2007,...).


	
	cuantasCategorias(X,Y,Resultado), que indica en Resultado cuántos niveles intermedios hay en el grafo entre X e Y en Resultado.
	Ejemplo de consulta: ?- cuantasCategorias(wv2010,herramientas,Resultado), que devuelve 4 (word, procesadortxt, oficina,herramientas).

	Nota: no he añadido corte en estas funciones para que evalúe recursivamente todos los hechos. Por ejemplo, si no añado corte, la consulta
        ?- cuantasCategorias(X,herramientas,Res).

	Me devuelve todas las posibles combinaciones (oficina 1, programación, 1...). Sin embargo, si añado corte, se queda en oficina 1, ya que ha encontrado un hecho con el que unifica. Este corte se tendría que añadir a la línea 78:
	cuantasCategorias(X,Y,1):-pertenece(X,Y),!.

*/

perteneceCategoria(X,Y):-pertenece(X,Y).
perteneceCategoria(X,Y):-pertenece(X,Z),perteneceCategoria(Z,Y).


cuantasCategorias(X,Y,1):-pertenece(X,Y).
cuantasCategorias(X,Y,Resultado):-pertenece(X,Z),cuantasCategorias(Z,Y,Tmp0),Resultado is Tmp0+1.


